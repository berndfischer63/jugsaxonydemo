#!/bin/bash

echo "############################################################"
echo "### start-dockerreg-container.sh => begin"
echo "###"

docker run \
       -d  \
       -p 5000:5000 \
       --name=repo  \
       registry

docker run \
       -d  \
       -p 9080:8080   \
       --name=repo-ui \
       atcol/docker-registry-ui

docker run \
       -d  \
       -p 9081:80 \
       -e ENV_DOCKER_REGISTRY_HOST=192.168.59.103 \
       -e ENV_DOCKER_REGISTRY_PORT=5000           \
       --name=repo-ui2 \
       konradkleine/docker-registry-frontend

echo "###"
echo "### start-dockerreg-container.sh => finished"
echo "############################################################"
