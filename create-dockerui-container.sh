#!/bin/bash

echo "############################################################"
echo "### start-dockerui-container.sh => begin"
echo "###"

docker run -d -p 9000:9000 -v /var/run/docker.sock:/docker.sock crosbymichael/dockerui -e /docker.sock

echo "###"
echo "### start-dockerui-container.sh => finished"
echo "############################################################"