#!/bin/bash

echo "############################################################"
echo "### start-mysql-container.sh => begin"
echo "###"

docker run --name=mysql \
	-e MYSQL_ROOT_PASSWORD=9876 \
	-e MYSQL_USER=test          \
	-e MYSQL_PASSWORD=1234     \
	-e MYSQL_DATABASE=jugdemo \
	-p 3306:3306 \
	-d mysql

echo "###"
echo "### start-mysql-container.sh => finished"
echo "############################################################"