package de.ma.jugdemo.cmn;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BaseEntity implements Serializable
{
	@Id
	protected String id;
	
	public BaseEntity()
	{
		this.id = UUID.randomUUID().toString();
	}
	
	public BaseEntity( String id )
	{
		if( id == null || id.length() == 0 )
		{
			this.id = UUID.randomUUID().toString();
		}
		else
		{
			this.id = id;
		}
	}

	public String getId(){ return this.id; }

	@Override
	public boolean equals( Object obj )
	{
		if( this == obj )
		{
			return true;
		}

		if( obj == null )
		{
			return false;
		}

		if( getClass() != obj.getClass() )
		{
			return false;
		}

		BaseEntity other = ( BaseEntity ) obj;
		if( this.id == null )
		{
			if( other.id != null ) return false;
		}
		else if( !this.id.equals( other.id ) )
		{
			return false;
		}

		return true;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this.id == null ) ? 0 : this.id.hashCode() );
		return result;
	}
	
	private static final long serialVersionUID = 1L;
}
