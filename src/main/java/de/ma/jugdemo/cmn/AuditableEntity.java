package de.ma.jugdemo.cmn;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.joda.time.LocalDateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.domain.Persistable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@MappedSuperclass
@EntityListeners( AuditingEntityListener.class )
public class AuditableEntity extends BaseEntity implements Persistable<String>
{
	private static final long serialVersionUID = 1L;

	public AuditableEntity(){ super(); }
	public AuditableEntity( AuditableEntity e )
	{
		if( e == null )
		{
			throw new IllegalArgumentException( "argument AuditableEntity must not be null" );
		}

		if( e.id == null || e.id.length() == 0 )
		{
			throw new IllegalArgumentException( "id of argument AuditableEntity must not be null" );
		}

		this.id                = e.id;
		this.createdTimestamp  = e.createdTimestamp;
		this.modifiedTimestamp = e.modifiedTimestamp;
	}

	// --------------------------------------------------------------------------------------------------------

	@Override
	public boolean isNew()
	{
		boolean retVal = false;

		if( this.createdTimestamp == null )
		{
			retVal = true;
		}

		return retVal;
	}

	// --------------------------------------------------------------------------------------------------------

	@Temporal( TemporalType.TIMESTAMP ) @CreatedDate @Basic( optional = true ) protected Date createdTimestamp;

	public LocalDateTime getCreatedTimestamp()
	{
		return Utils.convertUtilDateToLocalDateTime( this.createdTimestamp );
	}

	// ---

	@Temporal( TemporalType.TIMESTAMP ) @LastModifiedDate @Basic( optional = true ) protected Date modifiedTimestamp;

	public LocalDateTime getModifiedTimestamp()
	{
		return  Utils.convertUtilDateToLocalDateTime( this.modifiedTimestamp );
	}
}
