package de.ma.jugdemo.cmn;

import java.util.Date;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

public class Utils
{
	public static Date convertLocalDateToUtilDate( LocalDate date )
	{
		Date retVal = null;
		
		if( date != null )
		{
			retVal = date.toDate();
		}
		
		return retVal;
	}
	
	public static LocalDate convertUtilDateToLocalDate( Date date )
	{
		return ( date == null ) ? null : new LocalDate( date );
	}

	public static Date convertLocalDateTimeToUtilDate( LocalDateTime datetime )
	{
		Date retVal = null;
		
		if( datetime != null )
		{
			retVal = datetime.toDate();
		}
		
		return retVal;
	}
	
	public static LocalDateTime convertUtilDateToLocalDateTime( Date date )
	{
		return ( date == null ) ? null : new LocalDateTime( date );
	}
}
