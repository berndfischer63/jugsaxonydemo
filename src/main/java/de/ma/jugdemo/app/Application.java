package de.ma.jugdemo.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import de.ma.jugdemo.todo.Todo;
import de.ma.jugdemo.todo.TodoRepository;

@Configuration
@ComponentScan
@EnableAutoConfiguration
@EntityScan( basePackageClasses = Todo.class )
@EnableJpaRepositories( basePackageClasses = TodoRepository.class )
public class Application
{
	@SuppressWarnings("resource")
	public static void main( String[] args )
	{
		ConfigurableApplicationContext ctxt = SpringApplication.run( Application.class, args );
		TodoRepository                 repo = ctxt.getBean( TodoRepository.class );

		repo.save( new Todo( "Task1" ) );
		repo.save( new Todo( "Task2" ) );
		repo.save( new Todo( "Task3" ) );
		repo.save( new Todo( "Task4" ) );
		repo.save( new Todo( "Task5" ) );

		System.out.println( "===== TodoList ===============================================================" );

		Iterable<Todo> todoList = repo.findAll();
		for( Todo todo : todoList )
		{
			System.out.println( "Task: " + todo.getTitle() );
		}

		System.out.println( "==============================================================================" );

		ctxt.close();
	}
}
