package de.ma.jugdemo.todo;

import javax.persistence.Entity;

import de.ma.jugdemo.cmn.AuditableEntity;

@Entity
public class Todo extends AuditableEntity
{
	private static final long serialVersionUID = 1L;

	private String title;
	private String content;

	protected Todo()
	{
		//
	}

	public Todo( String title )
	{
		this.title   = title;
		this.content = null;
	}

	public String getTitle()
	{
		return this.title;
	}

	public void setTitle( String title )
	{
		this.title = title;
	}

	public String getContent()
	{
		return this.content;
	}

	public void setContent( String content )
	{
		this.content = content;
	}
}
