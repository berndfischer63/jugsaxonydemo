#!/bin/bash

echo "############################################################"
echo "### install_ansible.sh => begin"
echo "###"

if [ ! -f /usr/local/bin/ansible ]; then
  echo "Refreshing package list ..."
  apt-get update

  echo "Installing ansible ..."
  apt-get install python python-pip python-dev -y  

  pip install setuptools --upgrade
  pip install PyYAML jinja2 paramiko markupsafe
  pip install ansible
fi

if [ ! -d /etc/ansible ]; then
  mkdir /etc/ansible
fi

if [ ! -f /etc/ansible/hosts ]; then
  echo "localhost" > /etc/ansible/hosts
  chmod -x /etc/ansible/hosts
fi
echo "###"
echo "### install_ansible.sh => finished"
echo "############################################################"
