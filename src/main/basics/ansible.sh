#!/bin/bash

echo "############################################################"
echo "### ansible.sh => begin"
echo "###"

ANSIBLE_PLAYBOOK=$1
ANSIBLE_HOSTS=$2
ANSIBLE_VERBOSE=$3
TEMP_HOSTS="/tmp/ansible_hosts"

echo "Running Ansible provisioner defined in Vagrantfile."
echo "Running Ansible playbook $ANSIBLE_PLAYBOOK"

if [ ! -f /vagrant/$ANSIBLE_PLAYBOOK ]; then
  echo "Cannot find Ansible playbook."
  exit 1
fi

if [ ! -f /vagrant/$ANSIBLE_HOSTS ]; then
  echo "Cannot find Ansible hosts."
  exit 2
fi

cp /vagrant/${ANSIBLE_HOSTS} ${TEMP_HOSTS} && chmod -x ${TEMP_HOSTS}

if [ ! -s $ANSIBLE_VERBOSE ]; then
ansible-playbook /vagrant/${ANSIBLE_PLAYBOOK} --inventory-file=${TEMP_HOSTS} --connection=local -$ANSIBLE_VERBOSE
else
ansible-playbook /vagrant/${ANSIBLE_PLAYBOOK} --inventory-file=${TEMP_HOSTS} --connection=local
fi

rm ${TEMP_HOSTS}

echo "###"
echo "### ansible.sh => finished"
echo "############################################################"
