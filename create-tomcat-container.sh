#!/bin/bash

echo "############################################################"
echo "### start-tomcat-container.sh => begin"
echo "###"

for i in `seq 1 10`; do
echo "start container $i"
docker run -d -P -v $(pwd)/content:/var/lib/tomcat7/webapps/ROOT --name=tc$i befi/tomcat
done

echo "###"
echo "### start-tomcat-container.sh => finished"
echo "############################################################"
