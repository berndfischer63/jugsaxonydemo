#!/bin/bash

echo "############################################################"
echo "### image-parents.sh => begin"
echo "###"

curId=0933ef88b1a3

for i in {1..30}
do
  parId=$(docker inspect $curId | grep Parent | cut -c 16-27)
  if [[ $parId == \",* ]]; then
    echo "###"
    echo "### image-parents.sh => finished"
    echo "############################################################"
    exit
  fi
  echo "$curId child of $parId"
  curId=$parId
done

echo "###"
echo "### image-parents.sh => finished"
echo "############################################################"
