#!/bin/bash

echo "############################################################"
echo "### start-docker-container.sh => begin"
echo "###"

for i in `seq 1 20`; do
echo "start container $i"
docker run -d -P --name=nginx-instance$i nginx
done

echo "###"
echo "### start-docker-container.sh => finished"
echo "############################################################"
